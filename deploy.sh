#!/bin/bash
echo "Fetching $APP_VERSION image"
docker pull demo.goharbor.io/cobrainer-ci/microblog:$APP_VERSION

echo "Stopping old contianers"
docker stop microblog

echo "Starting new db container"
docker run --name mysql -d -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
    --log-driver=loki --log-opt loki-url="http://localhost:3100/loki/api/v1/push" \
    -e MYSQL_DATABASE=microblog -e MYSQL_USER=$DB_USER \
    -e MYSQL_PASSWORD=$DB_PASS \
    mysql/mysql-server:5.7

echo "Starting new app container"
docker run --name microblog -d -p 8000:5000 --rm --name microblog \
    --link mysql:dbserver \
    --log-driver=loki --log-opt loki-url="http://localhost:3100/loki/api/v1/push" \
    -e DATABASE_URL=mysql+pymysql://$DB_USER:$DB_PASS@dbserver/microblog \
    demo.goharbor.io/cobrainer-ci/microblog:$APP_VERSION
