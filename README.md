# Microblog with CI/CD and monitoring!

This project is an adaptation of [Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) where is used a Bitbucket Pipeline do run some tests, build a docker image and deploy in AWS.

![Login](./miscs/login.png)

## Requirements

- [Bitbucket](https://bitbucket.org/product)
- [Harbor](https://goharbor.io/)
- [AWS](https://aws.com/)
- [Ec2 repo](https://bitbucket.org/fhoust/ec2/src/master/)

## How to deploy

### Setting Harbor

In order to work, we will need a image repository, so go to [demo.goharbor](https://demo.goharbor.io/) and create a new account. This is a demo version of Harbor, it will least for only 24h. After the account creation follow this steps:

1. Create a new Project with public access
2. Enter in the new project and click in **Robot Accounts**
3. Create a robot account (Never expire and remove all delete permissions)
4. Save the Token and the name

### Setting an instance

If you need an instance:
- Clone the [Ec2 repo](https://bitbucket.org/fhoust/ec2/src/master/) and follow **How to deploy** tutorial

### Setting Bitbucket

Create an account in [Atlassian](https://www.atlassian.com/software/bitbucket/bundle) and create a Bitbucket Cloud account, then:

1. Create a new Project and a new repository
2. Clone this repo with public access and follow the instructions to import an existing repository
3. Go to **Repository settings** > **Pipelines** > **SSH keys**
4. Create a new key, copy the generated **Public key** and in **Known hosts** fill with your instance IP and click in **Fetch**
5. Paste the **Public key** in your instance under **~/.ssh/authorized_keys**
3. Go to **Repository settings** > **Pipelines** > **Repository variables** and add this variables:
    - DOCKER_HUB_USER: Is your robot account from GoHarbor
    - DOCKER_HUB_PASSWORD: Robot Account password
    - DB_USER: An user for your DB instance
    - DB_PASS: DB user password
    - SERVER_IP: Your instance IP

**OBS.**: Create all the variable with **Secured** assigned

## Running the pipeline

After you complete the **How to deploy** you will be able to run your pipeline. The pipeline will run automatic after each commit sent to Bitbucket, but if is necessary, you can it manually:

1. Go to **Pipelines**
2. Click in **Run pipeline**

## Versioning

You can versioning your project, just edit the file **version** in this repository. It will be used to generate a new image with a new tag.

## Observability

If you deployed the [Metrics repo](https://bitbucket.org/fhoust/metrics/src/master/), you can access this links to check metrics and logs:

- Grafana: http://<your_instance_ip>:3000
- Prometheus: http://<your_instance_ip>:9090
- Loki Api: http://<your_instance_ip>:3100
- Node_exporter: http://<your_instance_ip>:9100

## Troubleshooting

### No access to observability

Check your instance Security Group, your ip need to be defined in inboud rule

### Pipe Errors

You can check the status of every pipeline running and collect the errors

#### SSH Failed

1. Make sure the bitbucket key is correct set in **~/.ssh/authorized_keys**
2. Make sure **SERVER_IP** is the server IP
3. Check if your instance Security Group has access to Bitbucket in the port 22

### Build Erros

1. Check if your Harbor account is not expired. If so, create a new and change the robot variables in Bitbucket **pipeline variables**